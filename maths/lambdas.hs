{-# OPTIONS_GHC -Wall #-}
{-Ganesh Raman-}
square :: Integer -> Integer
square=(\x->x^(2::Integer))
multiple :: Integer -> Integer->Integer
multiple=(\x y->x*y)
--solving quadratic equations like ax^2+bx+c=0 in D form
quadratic :: (Double, Double, Double) -> (Double, Double)
quadratic=(\(a,b,c)->if(a==(0::Double)) then error "can only solve pure quadratic equations of degree =2"
                     else
                         let d=b^(2::Integer)-4*a*c in
                         if(d<0) then error "no real solutions possible"
						 else (
                               (-b + sqrt(d))/2*a,
							   (-b -sqrt(d))/2*a
                               ))