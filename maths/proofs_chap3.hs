{-# OPTIONS_GHC -Wall #-}
infix 1 ==>
(==>) :: Bool -> Bool -> Bool
x ==> y = (not x) || y
infix 1 <=>
(<=>) :: Bool -> Bool -> Bool
x <=> y = x == y
infix 2 <+>
(<+>) :: Bool->Bool->Bool
(<+>) a b=(a/=b)
--ex 3.7 if p => q then !q=>!p
{--
Logical proof:
Given p=>q
thus taking negation of negation given
not(not(p=>q))
not(p ^ not q)
not(not q ^ p)
which is as good as ^q=>^p 
 
--}
ex3_7::Bool
ex3_7= lhs==rhs
       where lhs=[if(not q) then (not p) else True|p<-[True,False],q<-[True,False] ]
             rhs=[if(p) then q else True|p<-[True,False],q<-[True,False]]
{--
ex 3.7 b 
Given P<=>Q then prove ^P<=>^Q
--}
ex_3_7_b :: Bool

ex_3_7_b=lhs==rhs
         where lhs=[(if p then q else True) && (if q then p else True) |p<-[True,False],q<-[True,False]]
               rhs=[(if (not p) then (not q) else True) && (if (not q) then (not p) else True) |p<-[True,False],q<-[True,False]]
{--
(p =>q) => p for all p and q
--}
ex_3_9 :: Bool
ex_3_9= and lhs
       where lhs=[(if (p_q p q) then p else True) == p|p<-[True,False],q<-[True,False]]
             p_q=(\p q -> if p then q else True)
{--
from p or q , neg p  it follows that q
--}
ex_3_9_1::Bool
ex_3_9_1=and lhs
         where lhs=[(p||q)==q|p<-[False],q<-[True,False]]
{--
Deduction rule example : P=>Q , Q=>R then prove that P=>R
--}
deduction_rule_example_proof :: Bool
deduction_rule_example_proof=and rhs
                             where 
                                   rhs=[ (((p==>q) && (q==>r))==>(p==>r))|p<-[True,False],q<-[True,False],r<-[True,False]]